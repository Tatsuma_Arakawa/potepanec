require 'rails_helper'

RSpec.describe "Products", type: :request do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon)   { create(:taxon) }

  describe 'GET/potepan/products/id' do
    before do
      get potepan_product_path(product.id)
    end

    it 'リクエストが成功する' do
      expect(response).to have_http_status "200"
    end

    it 'showテンプレートが表示される' do
      expect(response).to render_template :show
    end

    it '商品名が表示される' do
      expect(response.body).to include product.name
    end

    it '商品価格が表示される' do
      expect(response.body).to include product.display_price.to_s
    end

    it '商品説明が表示される' do
      expect(response.body).to include product.description
    end
  end
end
