require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) do
    create(:product, name: "sample_product", price: 19.99, description: "sample_description", taxons: [taxon])
  end

  before do
    visit potepan_product_path(product.id)
  end

  it "商品名、商品価格が表示される" do
    expect(page).to have_content "sample_product"
    expect(page).to have_content "$19.99"
    expect(page).to have_content "sample_description"
  end

  it "一覧ページへ戻るが正常に動作する" do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end
end
