require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy)        { create(:taxonomy, name: "taxonomy") }
  let(:taxon)           { create(:taxon, name: "sample_taxon", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:other_taxon)     { create(:taxon, name: "other_taxon", taxonomy: taxonomy) }
  let!(:other_product)  { create(:product, name: "other_product", price: 15.99, taxons: [other_taxon]) }
  let!(:product) do
    create(:product, name: "sample_product", price: 19.99, description: "sample_description", taxons: [taxon])
  end
  let!(:second_product) do
    create(:product, name: "second_product", price: 29.99, description: "second_description", taxons: [taxon])
  end

  before do
    visit potepan_category_path(taxon.id)
  end

  it "カテゴリ、商品件数が表示される" do
    expect(page).to have_content "sample_taxon"
    expect(page).to have_content "taxonomy"
    expect(page).to have_content taxon.products.count
  end

  it "sample_taxonをクリックすると、該当商品のみ表示される" do
    click_on 'sample_taxon'
    expect(current_path).to eq potepan_category_path(taxon.id)
    expect(page).to have_content "sample_product"
    expect(page).to have_content "second_product"
    expect(page).to have_content "$19.99"
    expect(page).to have_content "$29.99"
    expect(page).not_to have_content "other_product"
    expect(page).not_to have_content "$15.99"
  end

  it "商品をクリックすると商品詳細が表示される" do
    click_on "sample_product"
    expect(current_path).to eq potepan_product_path(product.id)
    expect(page).to have_content "sample_product"
    expect(page).to have_content "$19.99"
    expect(page).to have_content "sample_description"
    expect(page).not_to have_content "second_product"
    expect(page).not_to have_content "$29.99"
    expect(page).not_to have_content "second_description"
  end
end
